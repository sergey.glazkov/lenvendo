<?php 

class VideoLink
{
	public $url;
	protected $id;
	
	/* const _HOSTING
	/  Список видеохостингов (название, домены, шаблон адреса для iframe), новые хостинги и адреса добавлять сюда. 
	/  Другой интересный вариант реализации - сделать подгрузку из .csv или .xls файла, смогу. 
	*/
	const _HOSTING = [
		0 => [
			'name' => 'YouTube', 
			'hosts' => [
				'www.youtube.com', 
				'youtube.com', 
				'youtu.be',
			],
			'player' => 'https://www.youtube.com/embed/#VIDEO_ID#',
		],
		1 => [
			'name' => 'Vimeo', 
			'hosts' => [
				'vimeo.com',
			], 
			'player' => 'https://player.vimeo.com/video/#VIDEO_ID#',
		],
	];
	
	/*
	/  На вход конструктора принимаем строку адреса, сразу определяем хостинг 
	/  и сохраняем в поле ключ хостинга из списка _HOSTING. 
	/  Если хостинг не определен по адресу хоста в ссылке - завершаем работу скрипта. 
	/  Можно более мягкую обработку сделать, чтобы не останавливать весь скрипт. 
	*/
	function __construct(string $url)
	{
		$this->url = $url;
		$this->id = $this->getId();
		
		if($this->id < 0)
		{
			die('Неизвестный видеохостинг: ' . $this->url);
		}
	}
	
	/*
	/  Метод по адресу хоста в ссылке определяет 'условный id' хостинга из _HOSTING
	/  Этот id сохраняется в поле $id объекта
	*/
	protected function getId() : int
	{
		$host = parse_url($this->url, PHP_URL_HOST);
		foreach(self::_HOSTING as $index => $hosting)
			if(in_array($host, $hosting['hosts']))
				return $index;

		return -1;
	}
	
	/*
	/  Метод возвращает название хостинга
	*/
	public function getHostingName() : string
	{
		return self::_HOSTING[$this->id]['name'];
	}
	
	/*
	/  Метод возвращает идентификатор видео.
	/  Реализовано для ссылок, указанных в ТЗ. 
	/  Для ссылок с другим строением потребуется дополнять метод новыми условиями, иначе будет возвращать 'unknown'.
	*/
	public function getVideoId() : string
	{
		$parsedUrl = parse_url($this->url);
		
		if(isset($parsedUrl['query']))
		{
			// для ссылок первого типа с идентификатором в параметре запроса 'v'
			$queryParams = [];
			parse_str($parsedUrl['query'], $queryParams);
			if(isset($queryParams['v']))
				return $queryParams['v'];
		}
		
		if(isset($parsedUrl['path']))
		{
			// для ссылок другого типа с идентификатором в path-части ссылки
			return trim($parsedUrl['path'], '/');
		}
		
		return 'unknown';
	}
	
	/*
	/  Метод возвращает html-код DOM-элемента iframe с видео в содержимом.
	/  В параметрах принимает массив, элементы которого задают ширину, высоту и рамку фрейма. 
	*/
	public function getVideoIframe(array $params = []) : string
	{
		$width = isset($params['width']) ? (int)$params['width'] : 800; 
		$height = isset($params['height']) ? (int)$params['height'] : 600; 
		$frameborder = isset($params['frameborder']) ? max((int)$params['frameborder'], 1) : 0; 
		
		$html = '<iframe width="' . $width . '" height="' . $height . '" src="' . $this->getVideoPlayerAddress() . '" frameborder="' . $frameborder . '" allowfullscreen></iframe>';
		return $html;
	}
	
	/*
	/  Метод формирует из шаблона ссылку для атрибута 'src' фрейма
	*/
	protected function getVideoPlayerAddress() : string
	{
		return str_replace('#VIDEO_ID#', $this->getVideoId(), self::_HOSTING[$this->id]['player']);
	}
	
	/*
	/  Переопределение toString()
	*/
	public function __toString()
	{
		return '<pre>' . print_r($this, true) . '</pre>';
	}
}