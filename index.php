<?php require('classes/VideoLink.php');

// ссылки для примера
$links = [
	'https://www.youtube.com/watch?v=G1IbRujko-A',
	'https://youtu.be/homqyBxHwis',
	'https://vimeo.com/225408543',
];

// параметры iframe
$iframeParams = [
	'width' => 384,
	'height' => 240, 
	'frameborder' => 1,
];

foreach($links as $link)
{
	$obVideoLink = new VideoLink($link);
	echo '<b>Адрес: ' . $obVideoLink->url . '</b><br>';
	echo 'Хостинг: ' . $obVideoLink->getHostingName() . '<br>';
	echo 'ID видео: ' . $obVideoLink->getVideoId() . '<br>';
	echo 'iframe: <br>' . $obVideoLink->getVideoIframe($iframeParams) . '<br><br>';
}